import * as assert from 'assert';
import { resolve } from 'path';
import * as sinon from 'sinon';
import {
  runValidator,
} from '../src/runner';
import * as validator from '../src/validator';

const sandbox = sinon.createSandbox();
describe('runner', () => {
  let fakeValidateSchemaFiles: any;

  beforeEach(() => {
    fakeValidateSchemaFiles = sandbox.stub(validator, 'validateSchemaFiles');
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should run validator if two files are supplied', () => {
    process.argv = ['node', 'cli', 'resources/data.schema', 'resources/data.schema'];
    runValidator();
    sinon.assert.calledOnce(fakeValidateSchemaFiles);
  });

  it('should error out if only one file is supplied', () => {
    process.argv = ['node', 'cli', 'resources/data.schema'];
    assert.throws(runValidator, assert.AssertionError);
  });

  it('should run validator with whitelisted options specified', () => {
    process.argv = [
      'node',
      'cli',
      'resources/data.schema',
      'resources/data.schema',
      '--allowNewOneOf',
      '--allowNewEnumValue',
      '--allowReorder',
    ];
    runValidator();
    sinon.assert.calledWith(
      fakeValidateSchemaFiles,
      resolve('resources/data.schema'),
      resolve('resources/data.schema'),
      { allowNewOneOf: true, allowNewEnumValue: true, allowReorder: true },
    );
  });

  it('should error out if non-supported options are specified', () => {
    process.argv = [
      'node',
      'cli',
      'resources/data.schema',
      'resources/data.schema',
      '--allowSomethingNotSupported',
      '--allowNewEnumValue',
    ];
    assert.throws(runValidator, assert.AssertionError);
  });
});
