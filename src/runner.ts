import * as assert from 'assert';
import { resolve } from 'path';
import { validateSchemaFiles } from './validator';

const acceptedOpts = [
  '--allowNewOneOf',
  '--allowNewEnumValue',
  '--allowReorder',
];
const errString = `
Usage: json-schema-diff-validator schemafile1 schemafile2

Flags:

--allowNewOneOf - new oneOf/anyOf items are considered a backwards compatible change (false by default)
--allowNewEnumValue - new enum values are considered a backwards compatible change (false by default)
--allowReorder - reordering of anyOf items are considered a backwards compatible change (false by default)
`;

export function runValidator() {
  const args = process.argv.slice(2, 4);
  assert.strictEqual(args.length, 2, errString);
  const opts = process.argv.slice(4, 7);
  assert.ok(
    opts.every(opt => acceptedOpts.includes(opt)),
    errString,
  );
  const files = args.map(file => resolve(file));
  validateSchemaFiles(
    files[0],
    files[1],
    opts.reduce(
      (acc, curr) => ((acc[curr.slice(2)] = true), acc),
      {} as { [key: string]: boolean },
    ),
  );
}
